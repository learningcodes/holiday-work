import Vue from 'vue'
import App from './App.vue'
import router from './router/Router.js'
import store from './store/Store.js'
import './plugins/element.js'
import 'element-ui/lib/theme-chalk/index.css';
import './assets/Global/css/global.css'

// 导入富文本编辑器及其样式
import VueQuillEditor from 'vue-quill-editor'
import 'quill/dist/quill.core.css' // import styles
import 'quill/dist/quill.snow.css' // for snow theme
import 'quill/dist/quill.bubble.css' // for bubble theme

Vue.use(VueQuillEditor)

Vue.config.productionTip = false

// Vue.use(element)
new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
