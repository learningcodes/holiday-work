import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login.vue'
import Home from '../components/Home.vue'
import Passages from '../components/passage/Passages.vue'
import quillEditor from '../components/passage/quillEditor.vue'
import passageData from '../components/passage/passageData.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: '/login'
  },
  {
    path: '/login',
    component: Login
  },
  {
    path: '/home',
    component: Home,
    children: [
      { path: '/', redirect: '/passages' },
      { path: '/passages', component: Passages },
      { path: '/quillEditor', component: quillEditor },
      { path: '/passageData', component: passageData }
    ]
  }
]

const router = new VueRouter({
  routes
})

export default router
