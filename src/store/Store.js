import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    options: [
      {
        value: 'HTML',
        lable: 'HTML'
      },
      {
        value: 'CSS',
        lable: 'CSS'
      },
      {
        value: 'JavaScript',
        lable: 'JavaScript'
      },
      {
        value: 'Vue',
        lable: 'Vue'
      },
      {
        value: 'React',
        lable: 'React'
      },
      {
        value: 'Angular',
        lable: 'Angular'
      },
      {
        value: 'Node',
        lable: 'Node'
      },
    ],
    navList: [
      { title: '首页', index: '1'},
      { title: '管理',
        index: '2',
        children: [
          { title: '批量删除', index: '2-1'},
          { title: '移动', index: '2-2'},
          { title: '编辑', index: '2-3'},
        ]
      },
      { title: '发布', index: '3'}
    ],
    channels: ['HTMl', 'CSS', 'JavaScript', 'Vue', 'React', 'Angular', 'Node'],
    data: {
      html: {
        total: 7,
        passages: [
          {
            title: '学习HTMl',
            content: `
超文本标记语言（英语：HyperText Markup Language，简称：HTML）是一种用于创建网页的标准标记语言。您可以使用 HTML 来建立自己的 WEB 站点，HTML 运行在浏览器上，由浏览器来解析。 在本教程中，您将学习如何使用 HTML 来创建站点。HTML 很容易学习！
                        
相信您能很快学会它！`,
            createTime: 1614517538103,
            updataTime: 1614517600000,
            publishTime: 1614517600000,
            source: '原创',
            author: 'admin',
            summary: '超文本标记语言（英语：HyperText Markup Language，简称：HTML）是一种用于创建网页的标准标记语言。 您可以使用 HTML 来建立自己的 WEB 站点',
            clickScore: 99,
            replyCount: 0,
            recommend: false,
            headLine: false,
            keyWords: '新手 HTMl 学习',
            channels: 'html',
            user: 'admin'
          },
          {
            title: '学习HTMl',
            content: '超文本标记语言（英语：HyperText Markup Language，简称：HTML）是一种用于创建网页的标准标记语言。            您可以使用 HTML 来建立自己的 WEB 站点，HTML 运行在浏览器上，由浏览器来解析。 在本教程中，您将学习如何使用 HTML 来创建站点。HTML 很容易学习！相信您能很快学会它！',
            createTime: 1614517538103,
            updataTime: 1614517600000,
            publishTime: 1614517600000,
            source: '原创',
            author: 'admin',
            summary: '超文本标记语言（英语：HyperText Markup Language，简称：HTML）是一种用于创建网页的标准标记语言。 您可以使用 HTML 来建立自己的 WEB 站点',
            clickScore: 99,
            replyCount: 0,
            recommend: false,
            headLine: false,
            keyWords: '新手 HTMl 学习',
            channels: 'html',
            user: 'admin'
          },
          {
            title: '学习HTMl',
            content: '超文本标记语言（英语：HyperText Markup Language，简称：HTML）是一种用于创建网页的标准标记语言。            您可以使用 HTML 来建立自己的 WEB 站点，HTML 运行在浏览器上，由浏览器来解析。 在本教程中，您将学习如何使用 HTML 来创建站点。HTML 很容易学习！相信您能很快学会它！',
            createTime: 1614517538103,
            updataTime: 1614517600000,
            publishTime: 1614517600000,
            source: '原创',
            author: 'admin',
            summary: '超文本标记语言（英语：HyperText Markup Language，简称：HTML）是一种用于创建网页的标准标记语言。 您可以使用 HTML 来建立自己的 WEB 站点',
            clickScore: 99,
            replyCount: 0,
            recommend: false,
            headLine: false,
            keyWords: '新手 HTMl 学习',
            channels: 'html',
            user: 'admin'
          },
        ]
      },
      css: {
        total: '1',
        passages: [
          {
            title: '学习Css',
            content: '1234567890',
            createTime: 1614517538103,
            updataTime: 1614517600000,
            publishTime: 1614517600000,
            source: '原创',
            author: 'admin',
            summary: '1234567890',
            clickScore: '99',
            replyCount: 0,
            recommend: false,
            headLine: false,
            keyWords: '新手 Css 学习',
            channels: 'css',
            user: 'admin'
          }
        ]
      }
    }
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
